import xdrlib


class TelemetryChannel:
    A = 0
    B = 1
    C = 2
    D = 3
    E = 4
    F = 5


class DataFlow:
    ONLINE_PERMANENT = 0
    ONLINE_SINGLE = 1
    ONLINE_PERMANENT_DUMMY = 2
    OFFLINE_PERMANENT = 4
    OFFLINE_SINGLE = 5
    OFFLINE_PERMANENT_DUMMY = 6
    STOP_TM = 0x80


class TelemetryRequest:

    def __init__(
            self,
            channel=TelemetryChannel.A,
            buffered=0,
            data_flow=DataFlow.ONLINE_PERMANENT_DUMMY,
            flow_id=0
            ):
        self.channel = channel
        self.buffered = buffered
        self.data_flow = data_flow
        self.flow_id = flow_id

    def encode(self):
        msg = xdrlib.Packer()
        msg.pack_uint(self.channel)
        msg.pack_uint(self.buffered)
        msg.pack_uint(self.data_flow)
        for _ in range(0, 9):
            msg.pack_uint(0)

        # header
        header = xdrlib.Packer()
        header.pack_uint(1234567890)
        header.pack_uint(len(msg.get_buffer()) + 16)
        header.pack_uint(self.flow_id)

        # tail
        tail = xdrlib.Packer()
        tail.pack_int(-1234567890)

        pdu = header.get_buffer() + msg.get_buffer() + tail.get_buffer()
        return pdu


class TelemetryMessage:

    def __init__(
            self,
            time,
            data
            ):
        self.time = time
        self.data = data

    @classmethod
    def decode(cls, pdu):
        header = xdrlib.Unpacker(pdu[0:12])
        preamble = header.unpack_uint()
        if preamble != 1234567890:
            raise ValueError

        packet_size = header.unpack_uint()
        flow_id = header.unpack_uint()
        data_size = packet_size - 16
        if data_size % 4 != 0:
            raise ValueError

        msg = pdu[12:12 + data_size]

        trailer = xdrlib.Unpacker(pdu[-4:])
        postamble = trailer.unpack_int()
        if postamble != -1234567890:
            raise ValueError

        breakpoint()

        return cls(
            time,
            data
        )
