import logging
import socket
import sys


logger = logging.getLogger(__name__)


def sockerror_win32(e: socket.error, tmid: int) -> bool:
    """
    Logs socket errors for Windows.
    Returns True if it's a reason to close the connection.
    """

    # The Network is down -> Log the error and quit.
    if e.winerror == 10050:
        logger.error(f'TM Connection {tmid}: Dead network. Closing socket...')
        return True

    # The Network is unreachable -> Log the error and quit.
    elif e.winerror == 10051:
        logger.error(f'TM Connection {tmid}: Unreachable network. Closing socket...')
        return True

    # The Network dropped the connection.
    elif e.winerror == 10052:
        logger.error(f'TM Connection {tmid}: Connection dropped by network. Closing socket...')
        return True

    # Software aborted the connection.
    elif e.winerror == 10053:
        logger.error(f'TM Connection {tmid}: Connection dropped by software. Closing socket...')
        return True

    # Peer resetted the connection.
    elif e.winerror == 10054:
        logger.error(f'TM Connection {tmid}: Connection dropped by peer. Closing socket...')
        return True

    # Socket is already shut down.
    elif e.winerror == 10058:
        logger.error(f'TM Connection {tmid}: Connection is already shut down. Closing socket...')
        return True

    # The network dropped the connection.
    elif e.winerror == 10060:
        logger.error(f'TM Connection {tmid}: Connection timed out. Closing socket...')
        return True

    # The host is down.
    elif e.winerror == 10064:
        logger.error(f'TM Connection {tmid}: Host is down. Closing socket...')
        return True

    # Host is unreachable.
    elif e.winerror == 10060:
        logger.error(f'TM Connection {tmid}: Host is unreachable. Closing socket...')
        return True

    # Socket is already shutting down.
    elif e.winerror == 10101:
        logger.error(f'TM Connection {tmid}: Socket is shutting down. Closing socket...')
        return True

    else:
        return False


def sockerror_linux(e: socket.error, tmid: int) -> bool:
    """
    Logs socket errors for Linux.
    Returns True if it's a reason to close the connection.
    """

    logger.error(f'TM Connection {tmid}: Socket error: {e}')

    return True


if sys.platform.startswith("win32"):
    sockerror = sockerror_win32
elif sys.platform.startswith("linux"):
    sockerror = sockerror_linux
