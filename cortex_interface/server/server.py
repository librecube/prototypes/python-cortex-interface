import threading
import socketserver
import yaml

from .handlers import DefaultHandler, TCHandler, TMHandler

from cortex_interface import logger
from cortex_interface.common.messages import TelemetryPacket


class Channel:
    CH_TC = "CH_TC"
    CH_TM = "CH_TM"


class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass


class CortexUser:
    """
    Implementation of a user-gateway for the Cortex protocol.
    """

    def __init__(self, host="localhost"):
        self._servers = []
        self._host = host
        self._kill = False

    def process_channel_file(self, filename):
        with open(filename, 'r') as f:
            document = yaml.safe_load(f)

        for entry in document:
            # Build a new server abstraction.
            user_server = {}
            user_server['channel'] = entry['channel']
            user_server['port'] = entry['port']

            # Select the handler depending on the Channel being built.
            handler = DefaultHandler

            if entry['channel'] == Channel.CH_TC:
                handler = TCHandler

            if entry['channel'] == Channel.CH_TM:
                handler = TMHandler

            # Build the TCP server.
            server = ThreadedTCPServer((self._host, entry['port']), handler)

            server.kill = False
            server.queuelock = threading.Lock()
            server.queues = {}

            if entry['channel'] == Channel.CH_TC:
                server.slots = 1
                server.ids = [i for i in range(0, 1)]
                logger.info('New Telecommand server configured.')

            elif entry['channel'] == Channel.CH_TM:
                server.slots = 24
                server.ids = [i for i in range(0, 24)]
                logger.info('New Telemetry server configured.')

            user_server['server'] = server

            # Prepare the server's thread.
            thread = threading.Thread(target=server.serve_forever)
            user_server['thread'] = thread

            # Append the new server to the Cortex Server list.
            self._servers.append(user_server)

    def startup(self):
        """Starts all the configured Cortex servers."""

        for server in self._servers:
            server['thread'].start()

    def shutdown(self):
        """Stops all Cortex servers."""
        for server in self._servers:
            # Send the kill signal to all the servers.
            server['server'].kill = True

            # Start the shutdown of the server.
            server['server'].shutdown()

        self.kill = True

    def frame_indication(self, sle, pdu):
        """Method to receive PDUs."""

        # Filter out non RAF instances.
        # Ignore check for now.
        # if isinstance(sle, RafServer)

        # Select the Telemetry server and push the PDU into the queues.
        server = self._get_server(Channel.CH_TM)

        if server is not None:
            self.add_frame_to_queue(server['server'], pdu)

    def add_frame_to_queue(self, server, pdu):
        """Pushes the given frame to the given server's queues."""

        # Build the Cortex packet.
        packet = TelemetryPacket.build(pdu)

        # Acquire the server queuelock to avoid data races.
        with server.queuelock:

            # Push the new PDU into all the currently registered queues.
            for queue in server.queues.values():
                queue.put(packet)

    def _get_server(self, channel: Channel):
        for server in self._servers:
            if server['channel'] == channel:
                return server
