import socket
import socketserver
import threading
import select
import queue
from datetime import datetime


from . import logger
from . import cortex as crt


class TelemetryMessageInterfaceClient:

    def __init__(self, host, port=3070, buffer_size=16384):
        self.host = host
        self.port = port
        self.buffer_size = buffer_size

    def connect(self):
        self.client_thread = threading.Thread(target=self._loop)
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.host, self.port))

    def disconnect(self):
        self.socket.close()

    def start(self):
        pdu = crt.TelemetryRequest().encode()
        self.socket.send(pdu)
        self.client_thread.kill = False
        self.client_thread.start()

    def stop(self):
        pdu = crt.TelemetryRequest(data_flow=crt.DataFlow.STOP_TM).encode()
        self.socket.send(pdu)
        self.client_thread.kill = True
        self.client_thread.join()

    def indication(self, message):
        # to be overwritten by user
        pass

    def _loop(self):
        thread = threading.currentThread()

        while not thread.kill:
            try:
                readable, _, _ = select.select([self.socket], [], [], 0)
            except ValueError:
                break

            for sock in readable:
                buffer = sock.recv(self.buffer_size)
                try:
                    message = crt.TelemetryMessage.decode(buffer)
                except ValueError:
                    break
                self.indication(message)






#
# class TelemetryMessageInterfaceServer:
#
#     def __init__(self, host, port):
#         self.host = host
#         self.port = port
#
#     def bind(self):
#         self.server = ThreadedTCPServer(
#             (self.host, self.port), ThreadedTCPRequestHandler)
#         self.server.client_queues = {}
#         self.server_thread = threading.Thread(target=self.server.serve_forever)
#         self.server.kill = False
#         self.server_thread.start()
#
#     def unbind(self):
#         self.server.kill = True
#         self.server.shutdown()
#
#     def request(self, message):
#         for q in self.server.client_queues.values():
#             q.put(message)
#
#
# class ThreadedTCPRequestHandler(socketserver.BaseRequestHandler):
#
#     # this method is entered once a client connects
#     def handle(self):
#
#         # keep track of connected clients and keep a queue for each
#         client_queue = queue.Queue()
#         self.server.client_queues[self.client_address] = client_queue
#
#         # run loop to send messages if available in queue
#         exit_loop = False
#         while True and not self.server.kill and not exit_loop:
#
#             while not client_queue.empty():
#                 message = client_queue.get()
#                 try:
#                     self.request.send(message.encode())
#                 except (
#                     BrokenPipeError,
#                     ConnectionAbortedError,
#                     ConnectionResetError
#                 ):
#                     # client disconnected
#                     exit_loop = True
#         # remove client from dict
#         del self.server.client_queues[self.client_address]
#
#
# class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
#     pass
#
