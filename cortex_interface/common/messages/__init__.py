import xdrlib
from .base import CortexMessage
from .telemetry_packet import TelemetryPacket
from .telemetry_request import TelemetryRequest


# Prebuilt message for Connection Rejection.
rejectPacker = xdrlib.Packer()
rejectPacker.pack_int(1234567890)
rejectPacker.pack_int(20)
rejectPacker.pack_int(0)
rejectPacker.pack_int(-2)
rejectPacker.pack_int(-1234567890)

REJECTION = rejectPacker.get_buffer()

# Prebuilt message for Unidentified Packet Error.
unidentifiedPacker = xdrlib.Packer()
unidentifiedPacker.pack_int(1234567890)
unidentifiedPacker.pack_int(20)
unidentifiedPacker.pack_int(0)
unidentifiedPacker.pack_int(-1)
unidentifiedPacker.pack_int(-1234567890)

UNIDENTIFIED = unidentifiedPacker.get_buffer()

# Prebuilt message for Packet Acknowledge message.
acknowledgePacker = xdrlib.Packer()
unidentifiedPacker.pack_int(1234567890)
unidentifiedPacker.pack_int(20)
unidentifiedPacker.pack_int(0)
unidentifiedPacker.pack_int(0)
unidentifiedPacker.pack_int(-1234567890)

ACKNOWLEDGE = acknowledgePacker.get_buffer()
