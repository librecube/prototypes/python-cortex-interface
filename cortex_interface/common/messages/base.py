import xdrlib

from cortex_interface import logger


class CortexMessage:
    """
    Base class of all Cortex messages.
    Handles the reception and validation of Cortex messages, as well as
    packing and formatting before sending.
    """

    def __init__(self, packet_size: int, flow_id: int, raw_data: bytes):
        """
        Builds a Cortex packet from the given data.
        """
        self.packet_size = packet_size
        self.flow_id = flow_id
        self._process(raw_data)

    @classmethod
    def recv(cls, connection):
        """
        Handles the reception of a possible Cortex message.
        WARNING: This method assumes the socket will not timeout
        and has data available to it.
        """

        logger.debug(f'Receiving a Cortex packet of type {cls}')

        # Receive the header and validate it.
        try:
            header = xdrlib.Unpacker(connection.recv(12))
        except EOFError:
            logger.error('Could not receive 12 bytes for Cortex Header.')
            raise EOFError

        # Check the Cortex header.
        if header.unpack_uint() != 1234567890:
            logger.error('Bad Cortex header checksum.')
            raise ValueError

        packet_size = header.unpack_uint()
        flow_id = header.unpack_uint()

        # Receive the contents of the packet.
        msgsize = packet_size - 16

        # Check the validity of the packet size.
        if (msgsize > 0) and ((msgsize % 4) == 0):
            try:
                logger.debug(f'Receiving {msgsize} bytes in message body.')
                msg = connection.recv(msgsize)
            except EOFError:
                logger.error(
                    f'Could not receive {msgsize} bytes for message body.')
                raise EOFError

        # If the packet size has an invalid avlue, raise an error.
        elif (msgsize % 4) != 0:
            logger.error(f'Message size of {msgsize} is not 32 bit aligned.')
            raise ValueError

        # Check the postamble.
        try:
            tail = xdrlib.Unpacker(connection.recv(4))
        except EOFError:
            logger.error('Could not receive 4 bytes for Cortex Postamble')
            raise EOFError

        if tail.unpack_int() != -1234567890:
            logger.error('Bad Cortex postamble checksum.')
            raise ValueError

        # Packet has been received and validated correctly.
        # Now porcess the message data.
        logger.debug('Building new packet from msg data.')
        output = cls(packet_size, flow_id, msg)
        logger.debug(f'New packet: {type(output)}')
        return output

    @staticmethod
    def pack(msg: bytes, flow_id=0) -> bytes:
        """
        Packs a raw Cortex message.
        """

        # Validate the message's size.
        if (len(msg) % 4) != 0:
            # TODO : Maybe log it??
            raise ValueError

        # Build the header.
        header = xdrlib.Packer()
        header.pack_uint(1234567890)
        header.pack_uint(len(msg) + 16)
        header.pack_uint(flow_id)

        # Build the poastamble.
        tail = xdrlib.Packer()
        tail.pack_int(-1234567890)

        buf = header.get_buffer()
        buf += msg
        buf += tail.get_buffer()

        return buf

    def _process(self, msg: bytes):
        """
        Processes the message's raw data into the instance's class data.
        To be overloaded by each class.
        """

        self.raw_data = msg
