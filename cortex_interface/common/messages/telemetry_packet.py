import logging
import xdrlib

from . import CortexMessage


logger = logging.getLogger(__name__)


class TelemetryPacket(CortexMessage):
    """
    Cortex packets encapsulating Telemetry frames.
    """

    @classmethod
    def build(cls, data: bytes, flow_id=0) -> bytes:
        """
        Packs the given data buffer into a Cortex packet.
        Cortex Header
        Telemetry metadata (6) (zeroed)
        Telemetry buffer size
        Telemetry metadata (4) (zeroed)
        Telemetry buffer (4-byte aligned)
        Cortex postamble
        """

        # Build the XDR Packer.
        xdr = xdrlib.Packer()

        # Pack the metadata.
        for _ in range(0, 7):
            xdr.pack_uint(0)

        xdr.pack_uint(len(data))

        for _ in range(0, 5):
            xdr.pack_uint(0)

        output = xdr.get_buffer()

        # Extend with the raw data.
        output += data

        # Pad until 4 byte aligned.
        while(len(output) % 4) != 0:
            output += b'0'

        return super().pack(output, flow_id)

    def _process(self, raw: bytes):
        """
        Overloaded method fror message processing.
        """

        # Build the unpacker.
        xdr = xdrlib.Unpacker(raw[0:52])

        # Time tag.
        timetag1 = xdr.unpack_uint()
        timetag2 = xdr.unpack_uint()

        # Sequence counter.
        counter = xdr.unpack_uint()

        # Frame check result.
        framecheck = xdr.unpack_uint()

        # Frame sync status.
        framesync = xdr.unpack_uint()

        # Bit slip status.
        bitslip = xdr.unpack_uint()

        # Telemetry delay.
        delay = xdr.unpack_uint()

        # Frame length.
        length = xdr.unpack_uint()

        # Sync word length.
        synclength = xdr.unpack_uint()

        # Decoder status.
        status1 = xdr.unpack_uint()

        # Other status
        status2 = xdr.unpack_uint()

        # Two unused words.
        _ = xdr.unpack_uint()
        _ = xdr.unpack_uint()

        if length > 0:
            self.tmdata = raw[52:(52+length)]
        else:
            self.tmdata = b''
