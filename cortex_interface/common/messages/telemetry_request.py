import xdrlib

from . import CortexMessage

from cortex_interface.common.constants import TMChannel


class TelemetryRequest(CortexMessage):
    """
    Representation of the Telemetry Request packets.
    """

    @classmethod
    def build(
            cls,
            channel: TMChannel,
            buffered: int,
            dataflow: int,
            flow_id: int
            ) -> bytes:
        """General build method for all Telemetry Requests"""

        # Build the packer.
        xdr = xdrlib.Packer()

        # Clamp the number of buffered frames.
        if buffered > 1024:
            buffered = 1024
        if buffered < 0:
            buffered = 0

        # Channel, buffered frames and data flow.
        xdr.pack_uint(channel)
        xdr.pack_uint(buffered)
        xdr.pack_uint(dataflow)

        # Pack other data.
        for _ in range(0, 9):
            xdr.pack_uint(0)

        buffer = super().pack(xdr.get_buffer(), flow_id=flow_id)
        print(buffer)

        return buffer

    def _process(self, msg: bytes):
        """
        Overloaded method fror message processing.
        """

        # Build the Unpacker.
        xdr = xdrlib.Unpacker(msg)

        # Unpack configuration.
        self.channel = xdr.unpack_uint()
        self.buffered = xdr.unpack_uint()
        self.dataflow = xdr.unpack_uint()

        for _ in range(0, 9):
            xdr.unpack_uint()
