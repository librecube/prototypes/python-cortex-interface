import logging
import time

from cortex_interface.telemetry_message import TelemetryMessageInterfaceClient


logging.basicConfig(level=logging.DEBUG)

host = input("Host [127.0.0.1]: ")
host = host if host != "" else "127.0.0.1"

port = input("Port [3070]: ")
port = int(port) if port != "" else 3070

client = TelemetryMessageInterfaceClient(host, port)
client.indication = lambda message: print(
    message.time,
    message.data.hex()[:16],
    "...",
    message.data.hex()[-16:],
    )
client.connect()
client.start()

try:
    while True:
        time.sleep(0.1)
except KeyboardInterrupt:
    pass

client.stop()
client.disconnect()
